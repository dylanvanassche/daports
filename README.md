# Dylan's own Alpine Linux packages

This repository provides a repo for Dylan's devices with custom Alpine Linux packages.
These packages aren't upstreamed as they may not fulfill the high standards 
Alpine Linux requires when packaging software.

# Add package

- Build it with `pmbootstrap build --arch=$ARCH <packages>`
- Copy `packages` directory of `pmbootstrap` to this repository
- Push and let Gitlab CI deploy the packages to the repository
